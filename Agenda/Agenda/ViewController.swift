//
//  ViewController.swift
//  Agenda
//
//  Created by Jorge luis Menco Jaraba on 8/12/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    var agendaSQLiteRepository: AgendaSQLiteRepository!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.agendaSQLiteRepository = AgendaSQLiteRepository()
        self.agendaSQLiteRepository.createDatabaseConnection()
    }
    
    
}

