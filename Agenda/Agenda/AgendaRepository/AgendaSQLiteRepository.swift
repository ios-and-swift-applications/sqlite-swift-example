//
//  AgendaSQLiteRepository.swift
//  Agenda
//
//  Created by Jorge luis Menco Jaraba on 8/12/19.
//  Copyright © 2019 Jorge luis Menco Jaraba. All rights reserved.
//

import Foundation
import SQLite
import SQLiteObjc

//connecting database
//read-write database
//creating tables
//colum constraint
//table constraint
//CRUD SQLite
//Insert
//update
//deleted
//filter
//transactions and savepoints
//altering the schema
//renaming tables
//add colunms
//indexes


struct Event {
    public let id = Expression<Int>("id")
    public let name = Expression<String>("name")
    public let description = Expression<String>("description")
    public let startDate = Expression<Date>("startDate")
    public let active = Expression<Bool>("active")
    public let email = Expression<String>("email")
    public let locationId = Expression<Int>("locationId")
}

struct Localtion {
    public let id = Expression<Int>("_id")
    public let place = Expression<String>("Place")
    public let address = Expression<String>("Address")
}

struct Product {
    public let id = Expression<Int>("id")
    public let name = Expression<String>("name")
    public let price = Expression<Double>("price")
}

final class AgendaSQLiteRepository {
    var databaseConnection: Connection?
    let events = Table("Events")
    let location = Table("Locations")
    let temporaryProducts = Table("temp_products")
    let oldLocaltion = Table("old_location")
    let databaseFileName: String = "agendaDb1.sqlite3"
    let eventColums = Event()
    let locationColums = Localtion()
    let productColums = Product()

    public func createDatabaseConnection() {
        let databaseFilePath = "\(NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0])/\(databaseFileName)"

        do {
//            self.databaseConnection = try Connection(.temporary)
            self.databaseConnection = try Connection(databaseFilePath)
            self.createTables()
            self.insertData()
            self.someFilters()
        } catch {
            print(error)
        }
    }

    private func createTables() {
        self.dropTables()
        self.createLocationTable()
        self.createEvenTable()
        self.temporalTable()
    }

    private func createEvenTable() {
        do {
            try self.databaseConnection?.run(events.create { t in
                t.column(eventColums.id, primaryKey: .autoincrement)
                t.column(eventColums.name)
                t.column(eventColums.description)
                t.column(eventColums.startDate)
                t.column(eventColums.active)
                t.column(eventColums.email, check: eventColums.email.like("%@%"))
                t.column(eventColums.locationId)

                t.foreignKey(eventColums.locationId, references: location, locationColums.id, delete: .setNull)
            })
        } catch {
            print("Error")
        }
    }

    private func createLocationTable() {
        do {
            try self.databaseConnection?.run(location.create { t in
                t.column(locationColums.id, primaryKey: .autoincrement)
                t.column(locationColums.place)
                t.column(locationColums.address)
            })
        } catch let Result.error(message, code, nil) where code == SQLITE_CREATE_TABLE {
            print("error creting table: \(message)")
        } catch let error {
            print("error: \(error)")
        }
    }

    // temporal table

    private func temporalTable() {
        do {
            try self.databaseConnection?.run(self.temporaryProducts.create(temporary: true, ifNotExists: true) { t in
                    t.column(productColums.id, primaryKey: .autoincrement)
                    t.column(productColums.name)
                    t.column(productColums.price)
                })
        } catch let Result.error(message, code, nil) where code == SQLITE_CREATE_TABLE {
            print("error creting table: \(message)")
        } catch let error {
            print("error: \(error)")
        }
    }

    //SQLite CRUD

    private func insertData() {
        self.inserLocations()
        self.insertEvents()
    }

    private func inserLocations() {
        do {
            try self.databaseConnection?.run(self.location.insert([
                self.locationColums.place <- "Condor labs",
                self.locationColums.address <- "Avenida las palmas"
                ]))

            try self.databaseConnection?.run(self.location.insert([
                self.locationColums.place <- "cartagena",
                self.locationColums.address <- "boca grande"
                ]))

            try self.databaseConnection?.run(self.location.insert([
                self.locationColums.place <- "teatro universidad de metellin",
                self.locationColums.address <- "U de M"
                ]))

            try self.databaseConnection?.run(self.location.insert([
                self.locationColums.place <- "Parque norte",
                self.locationColums.address <- "parque norte"
                ]))
            print("Insert locations succesfully")
        } catch let Result.error(message, code, nil) where code == SQLITE_CONSTRAINT {
            print("contraint failed \(message)")
        } catch let error {
            print("something wrong \(error)")
        }
    }

    private func insertEvents() {
        do {
            try self.databaseConnection?.transaction {
                let condorLocationId = self.getLocationByName("cartagena")
                try self.databaseConnection?.run(self.events.insert([
                    self.eventColums.name <- "Condor party",
                    self.eventColums.description <- "The company party in cartagena",
                    self.eventColums.startDate <- Date(),
                    self.eventColums.active <- true,
                    self.eventColums.email <- "condorlabs@.gmail.com",
                    self.eventColums.locationId <- condorLocationId
                    ]))
            }

            try self.databaseConnection?.transaction {
                let locationId = self.getLocationByName("Condor labs")
                try self.databaseConnection?.run(self.events.insert([
                    self.eventColums.name <- "IOS meeting",
                    self.eventColums.description <- "the ios meeting platform",
                    self.eventColums.startDate <- Date(),
                    self.eventColums.active <- true,
                    self.eventColums.email <- "condorlabs@.gmail.com",
                    self.eventColums.locationId <- locationId
                    ]))
            }

            try self.databaseConnection?.transaction {
                let locationId = self.getLocationByName("Parque norte")
                try self.databaseConnection?.run(self.events.insert([
                    self.eventColums.name <- "new year",
                    self.eventColums.description <- "celebration of new year",
                    self.eventColums.startDate <- Date(),
                    self.eventColums.active <- true,
                    self.eventColums.email <- "condorlabs@.gmail.com",
                    self.eventColums.locationId <- locationId
                    ]))
            }

            try self.databaseConnection?.transaction {
                let locationId = self.getLocationByName("teatro universidad de metellin")
                try self.databaseConnection?.run(self.events.insert([
                    self.eventColums.name <- "Navidad",
                    self.eventColums.description <- "noche buena",
                    self.eventColums.startDate <- Date(),
                    self.eventColums.active <- true,
                    self.eventColums.email <- "condorlabs@.gmail.com",
                    self.eventColums.locationId <- locationId
                    ]))
            }

            try self.databaseConnection?.transaction {
                let locationId = self.getLocationByName("teatro universidad de metellin")
                try self.databaseConnection?.run(self.events.insert([
                    self.eventColums.name <- "partido de liga colombia",
                    self.eventColums.description <- "the game in colombia league",
                    self.eventColums.startDate <- Date(),
                    self.eventColums.active <- true,
                    self.eventColums.email <- "condorlabs@.gmail.com",
                    self.eventColums.locationId <- locationId
                    ]))
            }
            print("Insercion correcta")
        } catch let Result.error(message, code, statement) where code == SQLITE_CONSTRAINT {
            print("contraint failed \(message), statement: \(String(describing: statement))")
        } catch let error {
            print("something wrong \(error)")
        }
    }

    private func getLocationByName(_ name: String) -> Int {
        let query = self.location.select(locationColums.id).where(locationColums.place == name).limit(1)
        var IdLocation: Int = 0
        do {
            let result = try self.databaseConnection?.prepare(query)
            for location in result! {
                IdLocation = location[locationColums.id]
            }
        } catch let Result.error(message, code, nil) where code == SQLITE_TRANSACTION {
            print("error in the transction: \(message), code: \(code)")
        } catch let error {
            print("something wrong \(error)")
        }

        return IdLocation
    }

    private func UpdtaeData() {

        // update all table

        do {
            try self.databaseConnection?.run(self.events.update([
                self.eventColums.email <- "email123@gmail.com"
                ]))
        } catch let Result.error(message, code, nil) where code == SQLITE_UPDATE {
            print("Error in the update \(message): \(code)")
        } catch {
            print("General error: \(error)")
        }

        //update one record

        do {
            try self.databaseConnection?.transaction {
                let evetToUpdate = self.events.filter(self.eventColums.id == 1)
                try self.databaseConnection?.run(evetToUpdate.update(self.eventColums.active <- false))
            }
        } catch let Result.error(message, code, nil) where code == SQLITE_UPDATE {
            print("Error in the update \(message): \(code)")
        } catch {
            print("General error: \(error)")
        }
    }

    private func someFilters() {
        // Scalars

        do {
            let totalEvents = try self.databaseConnection?.scalar(self.events.count)
            print("total events: \(String(describing: totalEvents))")

            let idMax = try self.databaseConnection?.scalar(self.events.select(self.eventColums.id.max))
            print("id max: \(String(describing: idMax))")

            //Inner join
            let locationId = self.getLocationByName("teatro universidad de metellin")
            let joinQuey = events.filter(eventColums.locationId == locationId).join(location, on: locationColums.id == eventColums.locationId)
            let result = try self.databaseConnection?.prepare(joinQuey)

            for event in result! {
                print("name: \(event[eventColums.name]), description: \(event[eventColums.description])")
            }

        } catch let Result.error(message, code, nil) where code == SQLITE_TRANSACTION {
            print("error in the transction: \(message), code: \(code)")
        } catch let error {
            print("something wrong \(error)")
        }
        
        //orders
        let orderQuery = self.events.order(self.eventColums.name.desc, self.eventColums.email.asc)
        try! self.databaseConnection?.transaction {
            let result = try! self.databaseConnection?.prepare(orderQuery)
            result?.forEach({ (item) in
                print("\(item[eventColums.id]) \(item[eventColums.name]) \(item[eventColums.active])")
            })
        }
    }

    private func deleteData() {
        // delete alls events
//        do {
//            try self.databaseConnection?.run(self.events.delete())
//        } catch {
//            print(error)
//        }

        // delete one event

        do {
            try self.databaseConnection?.transaction {
                let event = self.events.filter(self.eventColums.name == "Condor labs")
                try self.databaseConnection?.run(event.delete())
            }
        } catch let Result.error(message, code, nil) where code == SQLITE_TRANSACTION {
            print("error in the transction: \(message), code: \(code)")
        } catch let error {
            print("an error occurred: \(error)")
        }

    }

    private func dropTables() {
        try! self.databaseConnection?.transaction {

            try! self.databaseConnection?.run(location.drop(ifExists: true))
            try! self.databaseConnection?.run(events.drop(ifExists: true))
        }
    }

    // indexes

    private func createIndexes() {
        do {
            try self.databaseConnection?.run(self.events.createIndex(self.eventColums.id))
            try self.databaseConnection?.run(self.events.createIndex(self.eventColums.name))
        } catch let Result.error(message, code, nil) where code == SQLITE_CREATE_INDEX {
            print("a error creating index: \(message)")
        } catch let error {
            print(error)
        }
    }

    private func renameTable() {
        do {
            try self.databaseConnection?.run(self.location.rename(self.oldLocaltion))
        } catch {
            print(error)
        }
    }
}
